Rails.application.routes.draw do

  #Root URL
  root "pages#index"

  get 'home', to: 'pages#home'
  get 'profile', to: 'pages#profile'
  get 'explore', to: 'pages#explore'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
